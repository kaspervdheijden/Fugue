<?php

declare(strict_types=1);

namespace Fugue\Logging;

use Fugue\IO\Stream\StreamWriterInterface;

final class StreamLogger extends Logger
{
    public function __construct(private readonly StreamWriterInterface $streamWriter)
    {
    }

    protected function log(string $logType, string $message): void
    {
        $this->streamWriter->write(
            $this->getFormattedMessage(
                $logType,
                $message,
            )
        );
    }
}
