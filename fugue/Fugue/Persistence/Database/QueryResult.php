<?php

declare(strict_types=1);

namespace Fugue\Persistence\Database;

final readonly class QueryResult
{
    public function __construct(
        private int $affectedRows,
        private ?string $insertedId,
    ) {
    }

    public function getAffectedRows(): int
    {
        return $this->affectedRows;
    }

    public function getInsertedId(): ?string
    {
        return $this->insertedId;
    }
}
