<?php

declare(strict_types=1);

namespace Fugue\Persistence\Database;

final readonly class DatabaseConnectionSettings
{
    public function __construct(
        private string $host,
        private string $user,
        private string $password,
        private string $charset,
        private string $timezone,
        private array $options,
    ) {
    }

    public function getHost(): string
    {
        return $this->host;
    }

    public function getUser(): string
    {
        return $this->user;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function getOptions(): array
    {
        return $this->options;
    }

    public function getCharset(): string
    {
        return $this->charset;
    }

    public function getTimezone(): string
    {
        return $this->timezone;
    }
}
