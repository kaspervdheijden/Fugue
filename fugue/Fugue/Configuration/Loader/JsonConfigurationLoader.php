<?php

declare(strict_types=1);

namespace Fugue\Configuration\Loader;

use JsonException;

use function file_get_contents;
use function json_decode;
use function is_array;

use const JSON_THROW_ON_ERROR;

final readonly class JsonConfigurationLoader extends FileConfigurationLoader
{
    protected function loadFromFile(string $filename): ?array
    {
        $contents = file_get_contents($filename);

        try {
            $json = json_decode($contents, true, 512, JSON_THROW_ON_ERROR);
            if (! is_array($json)) {
                return null;
            }
        } catch (JsonException) {
            return null;
        }

        return $json;
    }
}
