<?php

declare(strict_types=1);

namespace Fugue\Configuration\Loader;

final readonly class PHPConfigurationLoader extends FileConfigurationLoader
{
    protected function loadFromFile(string $filename): ?array
    {
        $result = require_once $filename;
        if ($result === null) {
            return null;
        }

        return (array)$result;
    }
}
