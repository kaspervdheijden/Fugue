<?php

declare(strict_types=1);

namespace Fugue\Command;

use Fugue\Container\Container;
use Fugue\Core\Kernel;

final readonly class CommandFactory
{
    public function __construct(
        private Kernel $kernel,
        private Container $container,
    ) {
    }

    public function getForIdentifier(string $identifier): CommandInterface
    {
        $command = $this->kernel->resolveClass($identifier, $this->container);
        if (! $command instanceof CommandInterface) {
            throw InvalidCommandException::forInvalidClassType($identifier);
        }

        return $command;
    }
}
