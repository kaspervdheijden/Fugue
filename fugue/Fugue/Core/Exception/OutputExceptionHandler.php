<?php

declare(strict_types=1);

namespace Fugue\Core\Exception;

use Fugue\Core\Output\OutputHandlerInterface;
use Throwable;

final readonly class OutputExceptionHandler extends ExceptionHandler
{
    public function __construct(private OutputHandlerInterface $outputHandler)
    {
    }

    public function handle(Throwable $throwable): void
    {
        $message = $this->formatExceptionMessage($throwable);
        $this->outputHandler->write($message);
    }
}
