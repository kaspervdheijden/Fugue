<?php

declare(strict_types=1);

namespace Fugue\Core\Output;

use Fugue\IO\Stream\StreamWriterInterface;

final readonly class StreamOutputHandler implements OutputHandlerInterface
{
    public function __construct(private StreamWriterInterface $streamWriter)
    {
    }

    public function write(string $text): void
    {
        $this->streamWriter->write($text);
    }
}
