<?php

declare(strict_types=1);

namespace Fugue\HTTP\Routing;

use Fugue\Collection\Collection;

final readonly class RouteMatchResult
{
    public function __construct(private Route $route, private Collection $arguments)
    {
    }

    public function getArguments(): Collection
    {
        return $this->arguments;
    }

    public function getRoute(): Route
    {
        return $this->route;
    }
}
