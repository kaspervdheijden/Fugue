<?php

declare(strict_types=1);

namespace Fugue\HTTP\Routing;

use Fugue\HTTP\Request;
use Closure;

final readonly class Route
{
    private function __construct(
        private string $name,
        private string $url,
        private ?string $method,
        private Closure|string $handler,
    ) {
    }

    /**
     * Binds a handler to a route, using an URL template.
     *
     * URL templates are a literals that matches an URL. E.G.:<br>
     * <code>/products/</code> will match <i>/products/</i>.
     *
     * Prefixed slashes are not mandatory. So this is equivalent:<br>
     * <code>products/</code> still matches <i>/products/</i>.
     *
     * In addition, variables are supported. Variables take the form {<i>name</i>},
     *  and are passed as an argument to the controller handler function. E.g.:<br>
     * <code>/product/{id}</code> matches <i>/product/what-ever</i>, and "what-ever" is passed to the controller.
     */
    public static function any(
        string $url,
        Closure|string $handler,
        string $name,
    ): self {
        return new self($name, $url, null, $handler);
    }

    public static function get(
        string $url,
        Closure|string $handler,
        string $name,
    ): self {
        return new self($name, $url, Request::METHOD_GET, $handler);
    }

    public static function post(
        string $url,
        Closure|string $handler,
        string $name,
    ): self {
        return new self($name, $url, Request::METHOD_POST, $handler);
    }

    public static function put(
        string $url,
        Closure|string $handler,
        string $name,
    ): self {
        return new self($name, $url, Request::METHOD_PUT, $handler);
    }

    public static function delete(
        string $url,
        Closure|string $handler,
        string $name,
    ): self {
        return new self($name, $url, Request::METHOD_DELETE, $handler);
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    public function getMethod(): ?string
    {
        return $this->method;
    }

    public function getHandler(): Closure|string
    {
        return $this->handler;
    }
}
