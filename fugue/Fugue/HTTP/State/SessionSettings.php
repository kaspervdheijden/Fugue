<?php

declare(strict_types=1);

namespace Fugue\HTTP\State;

final readonly class SessionSettings
{
    public function __construct(
        private string $name,
        private bool $useOnlyCookies,
        private int $cacheExpire,
        private bool $useCookies,
        private bool $httpOnly,
        private int $timeout,
        private bool $secure,
    ) {
    }

    public function shouldUseOnlyCookies(): bool
    {
        return $this->useOnlyCookies;
    }

    public function getCacheExpire(): int
    {
        return $this->cacheExpire;
    }

    public function shouldUseCookies(): bool
    {
        return $this->useCookies;
    }

    public function getHttpOnly(): bool
    {
        return $this->httpOnly;
    }

    public function isSecure(): bool
    {
        return $this->secure;
    }

    public function getTimeout(): int
    {
        return $this->timeout;
    }

    public function getName(): string
    {
        return $this->name;
    }
}
