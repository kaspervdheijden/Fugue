<?php

declare(strict_types=1);

if (! is_file(__DIR__ . '/../vendor/autoload.php')) {
    throw new  LogicException('Please run composer install before usage');
}

include_once __DIR__ . '/../vendor/autoload.php';
